package bahan;
import java.util.Scanner;

public class resep {
    public static void main(String[] args) {
        String identitas = "Dimas Angkasa / X RPL 6 / 19";
        System.out.println("Identitas : " + identitas);
        
        System.out.print("\nSaran Resep dari Bahan yang Anda miliki\n");
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Bahan Pertama: ");
        System.out.println("1. Pisang");
        System.out.println("2. Telur");
        System.out.print("Pilih bahan yang anda miliki (1/2) : ");
        int bahan1 = scanner.nextInt();
        
        if(bahan1 == 1 || bahan1 == 2){
            System.out.println("Bahan kedua : ");
            if(bahan1 == 1){
                System.out.println("1. Susu");
                System.out.println("2. Minyak Goreng");
                System.out.println("3. Tidak ada");
            }else {
                System.out.println("1. Minyak Goreng");
                System.out.println("2. Roti");
                System.out.println("3. Tidak ada");
            }
            System.out.print("Pilih bahan yang Anda Miliki (1/2/3) : ");
            int bahan2 = scanner.nextInt();
            
            if(bahan2 >= 1 && bahan2 <= 3){
                if(bahan1 == 1)
                    switch(bahan2)
                    {
                        case 1: System.out.println("Anda dapat membuat Banana Milk Shake"); break;
                        case 2: System.out.println("Anda dapat membuat Pisang Goreng"); break;
                        case 3: System.out.println("Anda dapat membuat Pisang Rebus");
                    }
                else
                    switch(bahan2){
                        case 1: System.out.println("Anda dapat membuat telur Mata Sapi"); break;
                        case 2: System.out.println("Anda dapat membuat Sandwich Telur"); break;
                        case 3: System.out.println("Anda dapat membuat Telur Rebus"); break;
                    }
            }else
                System.out.println("Mohon Maaf, Pilihan anda tidak tersedia, " + "Tidak dapat memberikan saran resep");
            
        }else
            System.out.println("Mohon Maaf, Pilihan anda tidak tersedia, " + "Tidak dapat memberikan saran resep");
    }
    
}
